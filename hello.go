package main

import (
	// "hello/task" // import from another package
	"hello/hellof"
	"html/template"
	"log"
	"net/http"
	"time"
)

// PageVariable stores variables in page in a struct
type PageVariable struct {
	Date string
	Time string
}

// homePage to show to time
func homePage(w http.ResponseWriter, r *http.Request) {

	now := time.Now()             // find the time right now
	HomePageVars := PageVariable{ // store the date and time in a struct
		now.Format("Mon Jan _2 2006"),
		now.Format("3:04PM"),
	}

	t, err := template.ParseFiles("templates/index.html") // parse the html file
	if err != nil {
		log.Print("template parsing error: ", err) // log it
	}

	err = t.Execute(w, HomePageVars) // execute the template and pass it the HomePageVars struct to fill in the gaps
	if err != nil {
		log.Print("template executing error: ", err)
	}

}

func main() {
	http.Handle("/static/css/", http.StripPrefix("/static/css/", http.FileServer(http.Dir("static/css"))))
	http.HandleFunc("/", homePage)
	http.HandleFunc("/todo/", hellof.ShowToDo)
	http.HandleFunc("/form/", hellof.AddMessage)
	http.ListenAndServe(":80", nil)
}
