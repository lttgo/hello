package hellof

import (
	"html/template"
	"net/http"
)

// ContactDetails describe data structure of contact message
type ContactDetails struct {
	Email   string
	Subject string
	Message string
}

// AddMessage receives message from HTML form
func AddMessage(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("templates/form.html"))

	details := ContactDetails{
		Email:   r.FormValue("email"),
		Subject: r.FormValue("subject"),
		Message: r.FormValue("message"),
	}

	_ = details

	if r.Method != http.MethodPost {
		tmpl.Execute(w, nil)
		return
	}

	tmpl.Execute(w, struct{ Success bool }{true})
}
