package hellof

import (
	"html/template"
	"net/http"
)

// Todo struct is Todo model
type Todo struct {
	Title string
	Done  bool
}

// TodoPageData contains template's variables
type TodoPageData struct {
	PageTitle string
	Todos     []Todo
}

// ShowToDo shows TODO Page
func ShowToDo(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("templates/task.html"))

	data := TodoPageData{
		PageTitle: "My TODO list",
		Todos: []Todo{
			{Title: "Task 1", Done: false},
			{Title: "Task 2", Done: true},
			{Title: "Task 3", Done: true},
		},
	}
	tmpl.Execute(w, data)

}
